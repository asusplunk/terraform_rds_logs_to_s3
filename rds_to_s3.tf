provider "aws" {}

resource "aws_s3_bucket" "rds_to_s3_bucket" {
  bucket_prefix = "asu.uto.rds.logs."
}

resource "aws_iam_role" "rds_to_s3_role" {
  name = "rds_to_s3"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "rds_to_s3_policy" {
  name = "rds_to_s3"
  role = "${aws_iam_role.rds_to_s3_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeRegions"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.rds_to_s3_bucket.id}/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.rds_to_s3_bucket.id}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "rds:DescribeDBInstances",
                "rds:DescribeDBLogFiles",
                "rds:DownloadDBLogFilePortion"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

resource "aws_lambda_function" "rds_to_s3_function" {
  function_name    = "rds_to_s3"
  filename         = "rds_to_s3.zip"
  handler          = "rds_to_s3.lambda_handler"
  runtime          = "python3.6"
  source_code_hash = "${base64sha256(file("rds_to_s3.zip"))}"
  role             = "${aws_iam_role.rds_to_s3_role.arn}"
  timeout          = 3600                                     # TODO: Timeout

  environment = {
    variables = {
      BUCKET = "${aws_s3_bucket.rds_to_s3_bucket.id}"
    }
  }
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_lambda" {
  statement_id  = "AllowExecutionFromCloudwatch"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.rds_to_s3_function.function_name}"
  principal     = "events.amazonaws.com"
  source_arn    = "${aws_cloudwatch_event_rule.hourly.arn}"
}

resource "aws_cloudwatch_event_rule" "hourly" {
  name                = "hourly"
  description         = "Fires every hour"
  schedule_expression = "rate(1 hour)"     # TODO: Frequency
}

resource "aws_cloudwatch_event_target" "rds_to_s3_function_hourly" {
  rule      = "${aws_cloudwatch_event_rule.hourly.name}"
  target_id = "${aws_lambda_function.rds_to_s3_function.function_name}"
  arn       = "${aws_lambda_function.rds_to_s3_function.arn}"
}
