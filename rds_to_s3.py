# Script to copy RDS logs into S3
# For issues with this script, contact Scott Odle <sjo@asu.edu>
# Adapted from https://github.com/ryanholland/rdslogs_to_s3/

import boto3
import botocore
import json
from datetime import datetime
from time import time
from os import environ

def lambda_handler(event, context):
    # Get the bucket ID from the environment variable
    bucket = environ['BUCKET']

    # Clients for EC2 (to list regions) and S3 (to read and write the bucket)
    ec2 = boto3.client('ec2')
    s3 = boto3.client('s3')

    # Look for RDS instances in all regions
    for region in ec2.describe_regions()['Regions']:
        # RDS clients are region-specific
        rds = boto3.client('rds', region_name=region['RegionName'])
        # Get logs for each instance in the region
        for instance in rds.describe_db_instances()['DBInstances']:
            log_dir = '{}_{}'.format(region['RegionName'], instance['DBInstanceIdentifier'])
            # Read the instance's checkpoint file if it has one, else create a default
            try:
                check_req = s3.get_object(Bucket=bucket, Key=log_dir + '/checkpoint.json')
                checkpoint = json.load(check_req['Body'])
            except botocore.exceptions.ClientError:
                checkpoint = {
                    'last_read': 0
                }

            # Get files with mod_times later than the checkpoint, then update the checkpoint
            logs = rds.describe_db_log_files(DBInstanceIdentifier=instance['DBInstanceIdentifier'], FileLastWritten=checkpoint['last_read'])
            checkpoint['last_read'] = int(time() * 1000)

            # Read each log
            for log in logs['DescribeDBLogFiles']:
                # Start reading the log from the checkpoint if it has one, else start from the top
                if log['LogFileName'] in checkpoint:
                    print('Warning - rewriting {}'.format(log['LogFileName']))
                    log_file = rds.download_db_log_file_portion(DBInstanceIdentifier=instance['DBInstanceIdentifier'], LogFileName=log['LogFileName'], Marker=str(checkpoint[log['LogFileName']]))
                else:
                    log_file = rds.download_db_log_file_portion(DBInstanceIdentifier=instance['DBInstanceIdentifier'], LogFileName=log['LogFileName'], Marker=str(0))

                # Read all new chunks of the log
                log_data = log_file['LogFileData']
                while log_file['AdditionalDataPending']:
                    log_file = rds.download_db_log_file_portion(DBInstanceIdentifier=instance['DBInstanceIdentifier'], LogFileName=log['LogFileName'], Marker=str(log_file['Marker']))
                    log_data += log_file['LogFileData']

                # Update the log file's checkpoint
                checkpoint[log['LogFileName']] = log_file['Marker']
                log_bytes = str.encode(log_data)

                # Put the log to S3
                s3.put_object(Bucket=bucket, Key=log_dir + '/' + log['LogFileName'], Body=log_bytes)

            # Put the new checkpoint file for the instance to S3
            checkpoint_json = str.encode(json.dumps(checkpoint))
            s3.put_object(Bucket=bucket, Key=log_dir + '/checkpoint.json', Body=checkpoint_json)

    # Lambda requires a return value
    return 0
