# Solution for copying Amazon RDS Logs into S3

## Included files
* `rds_to_s3.py`: Python3.6 Lambda code to iterate over all RDS instances in all regions of an AWS account and dump their logs into an S3 bucket.
* `rds_to_s3.tf`: Terraform script to create the Lambda, S3 bucket, schedule, and all associated roles and policies.

## Setup
* Pack the Lambda script into a zip file: `zip rds_to_s3.zip rds_to_s3.py`.
* If desired, update the timeout and schedule of the Lambda in the Terraform script.
* Set your AWS creds via environment variables or creds file.
* `terraform init`
* `terraform apply`

## Acknowledgements
This package is partially adapted from https://github.com/ryanholland/rdslogs_to_s3/